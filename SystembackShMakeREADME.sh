#SystembackSh.AppImage-master

SystembackShMake

wget https://gitlab.com/Chebur70/Appimage/-/raw/main/SystembackSh.AppImage-master.tar.gz

tar xvf SystembackSh.AppImage-master.tar.gz && rm -rf SystembackSh.AppImage-master.tar.gz

cd ~/SystembackSh.AppImage-master/app

make

chmod +x SystembackSh.AppImage

sudo cp SystembackSh.AppImage /usr/local/bin/

chmod +x AppDir/RestoreSystemback.desktop

chmod +x AppDir/BackUpSystemback.desktop

sudo cp AppDir/RestoreSystemback.desktop /usr/share/applications/

sudo cp AppDir/BackUpSystemback.desktop /usr/share/applications/

sudo cp AppDir/systemback.png /usr/share/icons/

cd ~

cp SystembackSh.AppImage-master/SystembackREADME.sh ~/'Рабочий стол'/    или    ~/'Desktop'/

sudo mkdir /home/sblive        
                                                
rm -Rf SystembackSh.AppImage-master

Чтобы удалить все точки восстановления, используйте следующие команды:
cd /home/sblive   
sudo chattr -Rfi SB_*   
sudo rm -rf SB_*  

 System backup and restore script for Debian-based distributions v3.4 by Kendek

  Available options:

   -n, --new [NAME]           create a new restore point
   -s, --storage              print the current storage directory path and the
                              slot
   -l, --list                 list available restore points
   -z, --size [INDEX]         calculate the incremental size of the storage slot
                              or the apparent size of a restore point
   -r, --restore [INDEX]      perform a system and/or user's configuration files
                              restoration
   -m, --repair [INDEX]       same as -r but the target (root) directory will be
                              the '/mnt' instead of the '/'
   -e, --rename INDEX [NAME]  rename a restore point
   -k, --keep INDEX           set the restore point to be manually removable
                              only
   -d, --remove [INDEX]       manually remove a restore point
